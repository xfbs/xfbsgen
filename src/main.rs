#![recursion_limit = "256"]

use chrono::{DateTime, Datelike, Utc};
use digest::Digest;
use git2::Repository;
use itertools::Itertools;
use log::{error, info, warn};
use maplit::hashset;
use pandoc::*;
use pandoc_ast::{MutVisitor, Pandoc as PandocAst};
use serde::Deserialize;
use std::collections::{HashMap, HashSet};
use std::path::*;
use std::sync::{Arc, RwLock};
use structopt::StructOpt;
use tempdir::TempDir;
use threadpool::ThreadPool;
use walkdir::WalkDir;

type HashAlgorithm = sha3::Sha3_256;

#[derive(Debug, StructOpt)]
#[structopt(name = "xfbsgen", about = "Static site generator for xfbs.net.")]
struct Options {
    /// Path to root of project where config.toml resides
    #[structopt(short, long, default_value = ".")]
    path: PathBuf,

    /// After generating, wait for file changes and regnerated if so
    #[structopt(short, long)]
    watch: bool,

    /// Serve files as HTTP
    #[structopt(short, long)]
    serve: Option<std::net::SocketAddr>,

    /// Compile files incrementally, skipping ones that don't need recompilation.
    #[structopt(short, long)]
    incremental: bool,

    /// Name of config file relative to path.
    #[structopt(short, long, default_value = "config.toml")]
    config: PathBuf,
}

#[derive(Deserialize, Clone)]
struct ConfigFeed {
    /// Only content from these paths will be processed
    paths: Vec<PathBuf>,
    /// Filename of RSS feed to be generated
    rss: Option<PathBuf>,
    /// Filename of atom feed to be generated
    atom: Option<PathBuf>,
    /// Title of feed
    title: String,
    /// Description of feed
    description: String,
}

#[derive(Deserialize, Clone)]
struct ConfigMeta {
    /// Default author
    author: String,
    /// Default footer.
    footer: String,
    /// DateTime formatting
    datetime: String,
    /// Language
    lang: String,
}

#[derive(Deserialize, Clone)]
struct ConfigPaths {
    /// Contents directory (containing markdown files).
    content: PathBuf,
    /// Output directory.
    output: PathBuf,
    /// Assets folder path (relative to output)
    assets: PathBuf,
}

#[derive(Deserialize, Clone)]
struct ConfigPages {
    /// Extension of markdown documents
    extension: String,
    /// How many threads to use (if not set, use cpu count).
    threads: Option<usize>,
    /// Filename for index file (usually 'index').
    index: String,
}

#[derive(Deserialize, Clone)]
struct ConfigTools {
    /// Which TeX engine to use to generate PDF.
    latex: String,
    /// PDF2SVG tool
    pdf2svg: String,
}

#[derive(Deserialize, Clone)]
struct ConfigHTML {
    /// Root dir where website is published ('/' or whatever).
    root: String,
    /// URL where website is published ('https://xfbs.net' or whatever).
    url: String,
    /// CSS Style Files to load
    css: HashSet<PathBuf>,
    /// JavaScript sources to load
    javascript: HashSet<PathBuf>,
    /// Minify HTML
    minify_html: bool,
    /// Logo to display
    logo: String,
    /// Navigation items
    navigation: Vec<ConfigNavigationItem>,
}

#[derive(Deserialize, Clone)]
struct ConfigNavigationItem {
    /// Name to display
    name: String,
    /// Where to link to
    target: PathBuf,
}

#[derive(Deserialize, Clone)]
struct Config {
    /// Paths to content and output
    paths: ConfigPaths,
    /// What to do about assets
    pages: ConfigPages,
    /// Config for HTML output
    html: ConfigHTML,
    /// Meta defaults
    meta: ConfigMeta,
    /// PDF config
    tools: ConfigTools,
    /// Feed generation config
    feed: ConfigFeed,
}

#[derive(Clone, Debug)]
struct ParsedPage {
    path: PathBuf,
    ast: PandocAst,
    dependencies: Arc<HashSet<PathBuf>>,
}

fn inline_to_string(value: &pandoc_ast::Inline) -> String {
    use pandoc_ast::Inline::*;
    match value {
        Str(s) => s.clone(),
        Space => " ".to_string(),
        _ => unreachable!(),
    }
}

fn meta_to_string(value: &pandoc_ast::MetaValue) -> String {
    use pandoc_ast::MetaValue::*;
    match value {
        MetaString(s) => s.clone(),
        MetaInlines(inlines) => {
            let strings: Vec<String> = inlines
                .iter()
                .map(|inline| inline_to_string(&inline))
                .collect();
            strings.join("")
        }
        _ => unreachable!(),
    }
}

impl ParsedPage {
    /// Extract keywords from a parsed page. This is hacky.
    pub fn keywords(&self) -> Option<Vec<String>> {
        use pandoc_ast::MetaValue::*;
        match self.ast.meta.get("keywords") {
            Some(MetaList(list)) => {
                let list = list.iter().map(|item| meta_to_string(item)).collect();
                Some(list)
            }
            Some(_) => unreachable!(),
            None => None,
        }
    }

    pub fn author(&self) -> Option<String> {
        self.meta_string("author")
    }

    pub fn title(&self) -> Option<String> {
        self.meta_string("title")
    }

    pub fn description(&self) -> Option<String> {
        self.meta_string("description")
    }

    pub fn published(&self) -> Option<DateTime<Utc>> {
        if let Some(published) = self.meta_string("published") {
            let naive_date = chrono::NaiveDate::parse_from_str(&published, "%Y-%m-%d").unwrap();
            let naive_datetime: chrono::NaiveDateTime = naive_date.and_hms(9, 40, 0);
            let datetime_utc = chrono::DateTime::<Utc>::from_utc(naive_datetime, Utc);
            return Some(datetime_utc);
        }

        None
    }

    pub fn meta_string(&self, name: &str) -> Option<String> {
        match self.ast.meta.get(name) {
            Some(value) => Some(meta_to_string(value)),
            None => None,
        }
    }
}

#[derive(Clone)]
struct StateRepo {
    /// Current commit
    commit: String,
}

impl StateRepo {
    fn new(path: &Path) -> Self {
        let repo = Repository::discover(&path).unwrap();
        let head = repo.head().unwrap();
        let commit = head.peel_to_commit().unwrap();
        let commit = hex::encode(commit.id().as_bytes());

        StateRepo { commit }
    }
}

#[derive(Clone, Debug)]
struct Dependency {
    /// Hash of this dependency
    hash: Vec<u8>,
    /// Pages which depend on this dependency
    pages: HashSet<PathBuf>,
    /// Paths to this dependency (could be multiple).
    paths: HashSet<PathBuf>,
}

impl Dependency {
    fn extensions(&self) -> HashSet<String> {
        let mut extensions = HashSet::new();
        for path in self.paths.iter() {
            let extension = path.extension().and_then(|e| e.to_str()).unwrap();
            extensions.insert(extension.to_string());
        }
        extensions
    }
}

#[derive(Clone)]
struct State {
    /// Config. Read-only and behind a reference count, so cloning is cheap.
    config: Arc<Config>,
    /// List of pages
    page_paths: Arc<RwLock<HashSet<PathBuf>>>,
    /// List of articles. Generated on-the-fly, and used to generate RSS and atom feeds.
    pages: Arc<RwLock<Vec<ParsedPage>>>,
    /// Repo state
    repo: Arc<StateRepo>,
    /// Options passed to command line
    options: Arc<Options>,
    /// Reverse mapping of dependencies
    dependencies_by_hash: Arc<RwLock<HashMap<Vec<u8>, Arc<RwLock<Dependency>>>>>,
    /// Lookup table of dependencies by path
    dependencies_by_path: Arc<RwLock<HashMap<PathBuf, Arc<RwLock<Dependency>>>>>,
}

impl State {
    pub fn new(options: Options, config: Config) -> Self {
        State {
            repo: Arc::new(StateRepo::new(&config.paths.content)),
            config: Arc::new(config),
            page_paths: Arc::new(RwLock::new(HashSet::new())),
            pages: Arc::new(RwLock::new(Vec::new())),
            options: Arc::new(options),
            dependencies_by_hash: Arc::new(RwLock::new(HashMap::new())),
            dependencies_by_path: Arc::new(RwLock::new(HashMap::new())),
        }
    }

    /// Real path to asset directory.
    fn assets_dir(&self) -> PathBuf {
        self.config.paths.output.join(&self.config.paths.assets)
    }

    fn html_target(&self, file: &Path) -> String {
        let file = file.with_extension("html");
        format!("{}{}", self.config.html.root, file.display())
    }

    fn html_target_url(&self, file: &Path) -> String {
        let file = file.with_extension("html");
        format!(
            "{}{}{}",
            self.config.html.url,
            self.config.html.root,
            file.display()
        )
    }

    /// Add metadata to markdown document used to generate HTML
    fn pandoc_add_menu(&self, pandoc: &mut pandoc_ast::Pandoc) {
        use pandoc_ast::MetaValue::*;
        pandoc.meta.insert(
            "homeurl".to_string(),
            MetaString(self.config.html.root.clone()),
        );

        pandoc.meta.insert(
            "logo".to_string(),
            MetaString(self.config.html.logo.clone()),
        );

        pandoc.meta.insert(
            "navigation".to_string(),
            MetaList(
                self.config
                    .html
                    .navigation
                    .iter()
                    .map(|item| {
                        let mut map = pandoc_ast::Map::new();
                        map.insert("name".into(), Box::new(MetaString(item.name.to_string())));
                        map.insert(
                            "target".into(),
                            Box::new(MetaString(self.html_target(&item.target))),
                        );
                        MetaMap(map)
                    })
                    .collect(),
            ),
        );
    }

    /// Fix metadata of markdown page
    fn pandoc_fix_meta(&self, pandoc: &mut pandoc_ast::Pandoc, file: &Path) {
        use pandoc_ast::MetaValue::*;
        if pandoc.meta.get("author").is_none() {
            pandoc.meta.insert(
                "author".to_string(),
                MetaString(self.config.meta.author.clone()),
            );
        }

        if pandoc.meta.get("title").is_none() {
            warn!("Missing title: {}", file.display());
        }

        if pandoc.meta.get("footer").is_none() {
            use pandoc_ast::Format;
            use pandoc_ast::Inline::*;
            pandoc.meta.insert(
                "footer".to_string(),
                MetaInlines(vec![RawInline(
                    Format("html".to_string()),
                    self.config.meta.footer.clone(),
                )]),
            );
        }

        if pandoc.meta.get("date").is_none() {
            let path = self.config.paths.content.join(file);
            let metadata = std::fs::metadata(path).unwrap();
            let modified = metadata.modified().unwrap();
            let datetime: DateTime<Utc> = modified.into();
            let formatted = datetime.format(&self.config.meta.datetime).to_string();
            pandoc
                .meta
                .insert("date".to_string(), MetaString(formatted));
        }

        if pandoc.meta.get("pdfurl").is_none() {
            let file = self.pdf_page_path(file);
            let url = format!("{}{}", &self.config.html.root, file.display());
            pandoc.meta.insert("pdfurl".into(), MetaString(url));
        }

        if let Some(rss) = &self.config.feed.rss {
            if pandoc.meta.get("rss").is_none() {
                pandoc.meta.insert(
                    "rss".into(),
                    MetaString(format!("{}{}", self.config.html.root, rss.display())),
                );
            }
        }

        if pandoc.meta.get("lang").is_none() {
            pandoc
                .meta
                .insert("lang".into(), MetaString(self.config.meta.lang.clone()));
        }

        // set commit
        pandoc
            .meta
            .insert("commit".into(), MetaString(self.repo.commit.clone()));
        pandoc
            .meta
            .insert("source".into(), MetaString(file.display().to_string()));
    }

    /// Process inline LaTeX code and turn it into figure
    fn pandoc_process_latex(&self, ast: &mut PandocAst) {}

    /// Add breadcrumbs metadata to AST
    fn pandoc_add_breadcrumbs(&self, ast: &mut PandocAst, file: &Path) {
        use pandoc_ast::MetaValue::*;
        if ast.meta.get("breadcrumbs").is_none() {
            let mut breadcrumbs = vec![];
            for ancestor in file.ancestors().collect::<Vec<_>>().iter().rev() {
                let current = ancestor == &file;
                let name = ancestor
                    .file_name()
                    .map(|f| f.to_str().unwrap())
                    .unwrap_or("Home");
                let name = if current {
                    name.to_string()
                } else {
                    inflector::cases::titlecase::to_title_case(name)
                };
                breadcrumbs.push((
                    name,
                    format!("{}{}", &self.config.html.root, ancestor.display()),
                    current,
                ));
            }
            breadcrumbs.last_mut().unwrap().1 = "#".to_string();

            ast.meta.insert(
                "breadcrumbs".to_string(),
                MetaList(
                    breadcrumbs
                        .into_iter()
                        .map(|(name, path, current)| {
                            let mut map = pandoc_ast::Map::new();
                            map.insert("name".to_string(), Box::new(MetaString(name)));
                            map.insert("url".to_string(), Box::new(MetaString(path)));
                            if current {
                                map.insert(
                                    "class".to_string(),
                                    Box::new(MetaString("is-active".to_string())),
                                );
                            }
                            MetaMap(map)
                        })
                        .collect(),
                ),
            );
        }
    }

    /// Pandoc default config
    fn get_pandoc_config(&self) -> Pandoc {
        let mut pandoc = pandoc::new();
        pandoc.set_number_sections();
        pandoc.add_option(PandocOption::Verbose);
        pandoc.add_option(PandocOption::Filter(PathBuf::from("pandoc-crossref")));
        pandoc.add_option(PandocOption::ResourcePath(vec![self
            .config
            .paths
            .output
            .clone()]));
        //pandoc.set_show_cmdline(true);
        pandoc.set_input_format(InputFormat::Json, vec![]);
        pandoc
    }

    /// Pandoc config for HTML pages
    fn get_pandoc_html_config(&self) -> Pandoc {
        let mut pandoc = self.get_pandoc_config();
        pandoc.set_output_format(OutputFormat::Html5, vec![]);
        pandoc.add_option(PandocOption::DataDir(PathBuf::from(".")));
        pandoc.add_option(PandocOption::Template(PathBuf::from("xfbs")));
        pandoc.add_option(PandocOption::MathML(None));

        // add all CSS files
        for file in &self.config.html.css {
            let dependency = self.dependency_by_path(file).unwrap();
            let dependency = dependency.read().unwrap();
            let asset = format!("{}.css", hex::encode(&dependency.hash));
            let path = format!(
                "{}{}",
                self.config.html.root,
                self.config.paths.assets.join(asset).display()
            );
            pandoc.add_option(PandocOption::Css(path));
        }

        // add all javascript files
        for file in &self.config.html.javascript {
            let dependency = self.dependency_by_path(file).unwrap();
            let dependency = dependency.read().unwrap();
            let asset = format!("{}.js", hex::encode(&dependency.hash));
            let path = format!(
                "{}{}",
                self.config.html.root,
                self.config.paths.assets.join(asset).display()
            );
            pandoc.add_option(PandocOption::Var("script".into(), Some(path)));
        }

        pandoc
    }

    /// Pandoc config for PDF pages
    fn get_pandoc_pdf_config(&self) -> Pandoc {
        let mut pandoc = self.get_pandoc_config();
        pandoc.add_option(PandocOption::DataDir(PathBuf::from(".")));
        pandoc.add_option(PandocOption::Template(PathBuf::from("xfbs")));
        pandoc.add_option(PandocOption::Meta("colorlinks".into(), None));
        pandoc.add_option(PandocOption::PdfEngine(PathBuf::from(
            &self.config.tools.latex,
        )));
        pandoc
    }

    /// Add page to index by path relative to content dir
    fn add_page(&self, path: &Path) {
        let path = path.to_path_buf();
        let mut pages = self.page_paths.write().unwrap();
        pages.insert(path);
    }

    /// Determine if something is a page. Path is relative to content dir.
    fn is_page(&self, path: &Path) -> bool {
        path.extension()
            .and_then(|e| e.to_str())
            .map(|e| e == self.config.pages.extension.as_str())
            .unwrap_or(false)
    }

    /// Scan content folder for content and resources.
    fn find_content(&self) {
        for entry in WalkDir::new(&self.config.paths.content) {
            let entry = entry.unwrap();

            // only care about files
            if !entry.file_type().is_file() {
                continue;
            }

            // get path relative to content
            let path = entry
                .path()
                .strip_prefix(&self.config.paths.content)
                .unwrap();

            if self.is_page(path) {
                self.add_page(path);
            }
        }
    }

    fn content_path(&self, path: &Path) -> PathBuf {
        self.config.paths.content.join(path)
    }

    fn add_dependency(&self, file: &Path, page: &Path) {
        if let Some(dependency) = self
            .dependencies_by_path
            .read()
            .unwrap()
            .get(file)
            .as_deref()
        {
            // this specific file is already known, we just note that this page
            // depends on it.
            let mut dependency = dependency.write().unwrap();
            dependency.pages.insert(page.to_path_buf());
            return;
        }

        // file is not known, so we compute hash to see if this content is
        // depended on elsewhere
        let hash = hash_file::<HashAlgorithm>(&self.content_path(file)).unwrap();
        if let Some(dependency) = self
            .dependencies_by_hash
            .read()
            .unwrap()
            .get(&hash)
            .as_deref()
        {
            // dependency already exists as another filename
            self.dependencies_by_path
                .write()
                .unwrap()
                .insert(file.to_path_buf(), dependency.clone());
            let mut dependency = dependency.write().unwrap();
            dependency.pages.insert(page.to_path_buf());
            dependency.paths.insert(file.to_path_buf());
            return;
        }

        // dependency (filename and content) is new
        let dependency = Arc::new(RwLock::new(Dependency {
            hash: hash.clone(),
            pages: hashset![page.to_path_buf()],
            paths: hashset![file.to_path_buf()],
        }));

        // insert into caches
        self.dependencies_by_path
            .write()
            .unwrap()
            .insert(file.to_path_buf(), dependency.clone());
        self.dependencies_by_hash
            .write()
            .unwrap()
            .insert(hash, dependency.clone());
    }

    fn find_dependencies(&self) {
        // go through list of pages
        let pages = self.pages.read().unwrap();
        for page in pages.iter() {
            let parent = page.path.parent().unwrap();
            for file in page.dependencies.iter() {
                let file = parent.join(file);
                if self.config.paths.content.join(file.clone()).is_file() {
                    self.add_dependency(&file, &page.path);
                }
            }
        }
    }

    fn generate_dependencies(&self) {
        // create assets folder
        let assets = self.assets_dir();
        std::fs::create_dir_all(assets).unwrap();

        let dependencies = self.dependencies_by_hash.read().unwrap();
        let pool = self.create_threadpool();
        for dependency in dependencies.values() {
            let dependency = dependency.clone();
            let state = self.clone();
            pool.execute(move || {
                state.generate_dependency(dependency);
            });
        }
        pool.join();
        assert_eq!(pool.panic_count(), 0);
    }

    fn generate_dependency(&self, dependency: Arc<RwLock<Dependency>>) {
        let dependency = dependency.read().unwrap();
        let extensions = dependency.extensions();
        for extension in extensions {
            // copy by default, but some files need additional processing.
            match extension.as_str() {
                "tex" => self.generate_dependency_tex(&dependency),
                _ => self.copy_dependency(&dependency, &extension),
            }
        }
    }

    fn generate_dependency_tex(&self, dependency: &Dependency) {
        let output = self
            .assets_dir()
            .join(format!("{}.pdf", hex::encode(&dependency.hash)));
        if !output.is_file() {
            info!("Generating {}", output.display());

            // generate pdf
            let path = dependency
                .paths
                .iter()
                .find(|e| e.extension().and_then(|e| e.to_str()) == Some("tex"))
                .unwrap();
            let path = self.config.paths.content.join(path);
            let tmp_dir = TempDir::new(&hex::encode(&dependency.hash)).unwrap();
            let tmp_dir_path = tmp_dir.path().display().to_string();
            let _result = std::process::Command::new(&self.config.tools.latex)
                .arg(format!("--output-directory={}", tmp_dir_path))
                .arg("--halt-on-error")
                .arg("--interaction=batchmode")
                .arg(&path)
                .output()
                .unwrap();
            let pdf_file = path.with_extension("pdf");
            let pdf_file = tmp_dir.path().join(pdf_file.file_name().unwrap());
            std::fs::copy(pdf_file, &output).unwrap();

            // generate svg
            let svg_file = output.with_extension("svg");
            let _result = std::process::Command::new(&self.config.tools.pdf2svg)
                .arg(output)
                .arg(svg_file)
                .output()
                .unwrap();
        }
    }

    fn copy_dependency(&self, dependency: &Dependency, extension: &str) {
        let output_path =
            self.assets_dir()
                .join(format!("{}.{}", hex::encode(&dependency.hash), extension));
        let file_path = self
            .config
            .paths
            .content
            .join(dependency.paths.iter().nth(0).unwrap());

        // copy only if not exists
        if !output_path.is_file() {
            info!(
                "Copying asset {} to {}",
                file_path.display(),
                output_path.display()
            );

            std::fs::copy(file_path, output_path).unwrap();
        }
    }

    /// Create a thread pool using the configuration.
    fn create_threadpool(&self) -> ThreadPool {
        let pool = threadpool::Builder::new();
        let pool = if let Some(threads) = self.config.pages.threads {
            pool.num_threads(threads)
        } else {
            pool
        };
        pool.build()
    }

    /// List of static dependencies (that all pages depend on).
    fn static_dependencies(&self) -> HashSet<PathBuf> {
        let mut dependencies = HashSet::new();
        for css in self.config.html.css.iter() {
            dependencies.insert(css.clone());
        }
        for js in self.config.html.javascript.iter() {
            dependencies.insert(js.clone());
        }
        dependencies
    }

    /// Given a path inside content, parse the page.
    fn parse_page(&self, path: &Path) -> ParsedPage {
        let file_path = self.config.paths.content.join(path);

        // configure pandoc for parsing
        let mut pandoc = pandoc::new();
        pandoc
            .add_input(&file_path)
            .set_output(OutputKind::Pipe)
            .set_output_format(OutputFormat::Json, vec![]);

        // extract parsed data
        let data = match pandoc.execute() {
            Ok(PandocOutput::ToBuffer(data)) => data,
            _ => panic!("error with pandoc output"),
        };

        // parse data
        let mut ast = PandocAst::from_json(&data);

        // static dependencies
        let mut dependencies = self.static_dependencies();
        use pandoc_ast::Inline::*;
        let mut visitor = InlineVisitor::new(|inline| match inline {
            Image(_, _, (target, _)) => {
                let path = PathBuf::from(&target);
                dependencies.insert(path);
            }
            _ => {}
        });
        visitor.walk_pandoc(&mut ast);

        let dependencies = Arc::new(dependencies);

        ParsedPage {
            path: path.to_path_buf(),
            ast,
            dependencies,
        }
    }

    /// Go through the list of discovered page paths, parse each page using
    /// pandoc, and save the result.
    fn parse_pages(&self) {
        let pool = self.create_threadpool();
        let page_paths = self.page_paths.read().unwrap();
        for path in page_paths.iter() {
            let path = path.to_path_buf();
            let config = self.clone();
            pool.execute(move || {
                let page = config.parse_page(&path);
                //let page = ParsedPage::new(&file_path, &path);
                let mut pages = config.pages.write().unwrap();
                pages.push(page);
            });
        }

        pool.join();
        assert_eq!(pool.panic_count(), 0);
    }

    /// Translate from path to markdown file to path to PDF file.
    fn pdf_page_path(&self, path: &Path) -> PathBuf {
        // instead of a path like /article/abc/index.pdf, we'd rather want /article/abc.pdf.
        // we check if this path is index.md, then we see if it has a parent,
        // and we make sure that parent.md doesn't already exist, as that would
        // be a clash. if all these are true, override the path.
        let mut path = path.to_path_buf();
        if path.file_name().and_then(|x| x.to_str()) == Some("index.md") {
            if let Some(parent) = path.parent() {
                if !parent.as_os_str().is_empty()
                    && !parent
                        .with_extension(&self.config.pages.extension)
                        .is_file()
                {
                    path = parent.to_path_buf();
                }
            }
        }

        // create output page path
        path.with_extension("pdf")
    }

    fn html_page_path(&self, path: &Path) -> PathBuf {
        path.with_extension("html")
    }

    fn pandoc_list_of_articles(&self) -> Vec<pandoc_ast::Block> {
        use pandoc_ast::Block::*;
        use pandoc_ast::Inline::*;

        // get a list of all (published) articles
        let mut articles = vec![];
        let pages = self.pages.read().unwrap();
        for page in pages.iter() {
            if let Some(_published) = page.published() {
                articles.push(page);
            }
        }

        // sort articles by published date
        articles.sort_by_key(|article| article.published().unwrap());
        articles.reverse();

        // get an iterator over groups of articles from the same year
        let grouped = articles
            .iter()
            .group_by(|article| article.published().unwrap().year());
        let mut output = Vec::new();

        // generate list of articles
        for (group, iter) in &grouped {
            // new header for every year
            output.push(Header(
                1,
                (
                    "section".to_string(),
                    vec!["unnumbered".to_string()],
                    vec![],
                ),
                vec![Str(group.to_string())],
            ));

            let mut items = Vec::new();
            for article in iter {
                let mut item = Vec::new();
                item.push(Link(
                    ("".to_string(), vec![], vec![]),
                    vec![Str(article.title().unwrap())],
                    (article.path.display().to_string(), "".to_string()),
                ));
                if let Some(description) = article.description() {
                    item.push(Str(":".to_string()));
                    for segment in description.split(" ") {
                        item.push(Space);
                        item.push(Str(segment.to_string()));
                    }
                }

                item.push(Str(".".to_string()));
                items.push(vec![Plain(item)]);
            }
            output.push(BulletList(items));
        }
        output
    }

    fn pandoc_macros(&self, ast: &mut PandocAst) {
        use pandoc_ast::Block;
        use pandoc_ast::Block::*;
        let blocks = ast
            .blocks
            .iter()
            .map(|item| {
                let item: Box<dyn Iterator<Item = Block>> = match item {
                    RawBlock(_, data) if data == "\\listofarticles" => {
                        Box::new(self.pandoc_list_of_articles().into_iter())
                    }
                    _ => Box::new(std::iter::once(item.clone())),
                };
                item
            })
            .flatten()
            .collect();
        ast.blocks = blocks;
    }

    fn dependency_by_path(&self, path: &Path) -> Option<Arc<RwLock<Dependency>>> {
        self.dependencies_by_path
            .read()
            .unwrap()
            .get(path)
            .map(|e| e.clone())
    }

    fn pandoc_pdf_fix_paths(&self, ast: &mut PandocAst, base: &Path) {
        use pandoc_ast::Inline::*;

        let mut visitor = InlineVisitor::new(|inline| match inline {
            Image(_, _, (url, _)) => {
                let mut path = PathBuf::from(&url);
                if !path.is_absolute() {
                    path = base.join(PathBuf::from(&url));
                }

                if let Some(dependency) = self.dependency_by_path(&path) {
                    let dependency = dependency.read().unwrap();
                    let extension = path.extension().and_then(|e| e.to_str()).unwrap();
                    let extension = match extension {
                        "tex" => "pdf",
                        _ => extension,
                    };

                    let mut asset = self.config.paths.assets.clone();
                    asset.push(format!("{}.{}", hex::encode(&dependency.hash), extension,));
                    *url = format!("{}", asset.display());
                }
            }
            Link(_, _, (url, _)) => {
                let mut path = PathBuf::from(&url);
                if !path.is_absolute() {
                    path = base.join(PathBuf::from(&url));
                }

                let page_paths = self.page_paths.read().unwrap();
                if page_paths.contains(&path) {
                    *url = self.html_target_url(&path);
                }
            }
            _ => {}
        });

        visitor.walk_pandoc(ast);
    }

    fn generate_page_pdf(&self, page: &ParsedPage) {
        let path = self.pdf_page_path(&page.path);
        let base = page.path.parent().unwrap();

        // create parent if not exists
        if let Some(folder) = self.config.paths.output.join(&path).parent() {
            if !folder.is_dir() {
                std::fs::create_dir_all(folder).unwrap();
            }
        }

        let mut ast = page.ast.clone();
        self.pandoc_macros(&mut ast);
        self.pandoc_pdf_fix_paths(&mut ast, base);
        self.pandoc_fix_meta(&mut ast, &page.path);

        let path = self.config.paths.output.join(path);
        info!("Generating {}", path.display());
        let mut pandoc = self.get_pandoc_pdf_config();
        pandoc.set_input(InputKind::Pipe(ast.to_json()));
        pandoc.set_output(OutputKind::File(path));
        pandoc.execute().unwrap();
    }

    fn pandoc_html_fix_paths(&self, ast: &mut PandocAst, base: &Path) {
        use pandoc_ast::Inline::*;

        let mut visitor = InlineVisitor::new(|inline| match inline {
            Image(_, _, (url, _)) => {
                let mut path = PathBuf::from(&url);
                if !path.is_absolute() {
                    path = base.join(PathBuf::from(&url));
                }

                if let Some(dependency) = self.dependency_by_path(&path) {
                    let dependency = dependency.read().unwrap();
                    let extension = path.extension().and_then(|e| e.to_str()).unwrap();
                    let extension = match extension {
                        "tex" => "svg",
                        _ => extension,
                    };

                    let mut asset = self.config.paths.assets.clone();
                    asset.push(format!("{}.{}", hex::encode(&dependency.hash), extension,));
                    *url = format!("{}{}", self.config.html.root, asset.display());
                }
            }
            Link(_, _, (url, _)) => {
                let mut path = PathBuf::from(&url);
                if !path.is_absolute() {
                    path = base.join(PathBuf::from(&url));
                }

                let page_paths = self.page_paths.read().unwrap();
                if page_paths.contains(&path) {
                    *url = self.html_target(&path);
                }
            }
            _ => {}
        });

        visitor.walk_pandoc(ast);
    }

    fn generate_page_html(&self, page: &ParsedPage) {
        let path = self.html_page_path(&page.path);
        let base = page.path.parent().unwrap();

        if let Some(folder) = self.config.paths.output.join(&path).parent() {
            if !folder.is_dir() {
                std::fs::create_dir_all(folder).unwrap();
            }
        }

        let path = self.config.paths.output.join(path);
        let mut ast = page.ast.clone();
        self.pandoc_macros(&mut ast);
        self.pandoc_add_breadcrumbs(&mut ast, &page.path);
        self.pandoc_html_fix_paths(&mut ast, base);
        self.pandoc_add_menu(&mut ast);
        self.pandoc_fix_meta(&mut ast, &page.path);

        info!("Generating {}", path.display());
        let mut pandoc = self.get_pandoc_html_config();
        pandoc.set_input(InputKind::Pipe(ast.to_json()));

        if self.config.html.minify_html {
            use minify_html::*;
            pandoc.set_output(OutputKind::Pipe);
            let mut result = pandoc.execute().unwrap();
            let result = match &mut result {
                PandocOutput::ToBuffer(string) => {
                    let cfg = &Cfg {
                        minify_js: false,
                        minify_css: false,
                    };
                    minify_html::in_place_str(string, cfg).unwrap()
                }
                _ => panic!("wrong encoding"),
            };
            std::fs::write(path, result).unwrap();
        } else {
            pandoc.set_output(OutputKind::File(path));
            pandoc.execute().unwrap();
        }
    }

    fn generate_pages(&self) {
        let pool = self.create_threadpool();
        let pages = self.pages.read().unwrap();

        for page in pages.iter() {
            // create root folder if not exists
            if let Some(folder) = self.config.paths.output.join(&page.path).parent() {
                if !folder.is_dir() {
                    std::fs::create_dir_all(folder).unwrap();
                }
            }

            {
                let config = self.clone();
                let page = page.clone();
                pool.execute(move || {
                    config.generate_page_html(&page);
                });
            }

            {
                let config = self.clone();
                let page = page.clone();
                pool.execute(move || {
                    config.generate_page_pdf(&page);
                });
            }
        }

        pool.join();
        assert_eq!(pool.panic_count(), 0);
    }

    fn rss_feed_items(&self) -> Vec<rss::Item> {
        let pages = self.pages.read().unwrap();
        let mut items = Vec::new();
        for page in pages.iter() {
            if let Some(published) = page.published() {
                let mut guid = rss::Guid::default();
                guid.set_value(self.html_target_url(&page.path));
                guid.set_permalink(true);
                let item = rss::ItemBuilder::default()
                    .title(page.title().unwrap())
                    .author(page.author().unwrap_or(self.config.meta.author.clone()))
                    .description(page.description().unwrap_or("".to_string()))
                    .link(self.html_target_url(&page.path))
                    .guid(guid)
                    .pub_date(published.to_rfc2822())
                    .build()
                    .unwrap();
                items.push(item);
            }
        }
        items
    }

    fn generate_rss(&self) {
        if let Some(rss) = &self.config.feed.rss {
            let rss = self.config.paths.output.join(rss);

            info!("Generating {}", rss.display());
            let items = self.rss_feed_items();
            let channel = rss::ChannelBuilder::default()
                .title(self.config.feed.title.clone())
                .link(self.config.html.url.clone())
                .description(self.config.feed.description.clone())
                .items(items)
                .build()
                .unwrap();
            let file = std::fs::OpenOptions::new()
                .create(true)
                .write(true)
                .open(rss)
                .unwrap();
            channel.write_to(file).unwrap();
        }
    }

    fn run(&self) {
        // start webserver early
        let state = self.clone();
        let serve = std::thread::spawn(move || {
            if state.options.serve.is_some() {
                state.serve();
            }
        });

        // load content
        self.find_content();

        // parse pages
        self.parse_pages();

        self.find_dependencies();
        self.generate_dependencies();

        // copy resources into assets folder
        //self.copy_resources();

        // generate HTML + PDF
        self.generate_pages();

        // generate feeds
        self.generate_rss();

        if self.options.watch {
            self.watch();
        }

        serve.join().unwrap();
    }

    fn serve(&self) {
        use iron::Iron;
        use mount::Mount;
        use staticfile::Static;
        let address = &self.options.serve.unwrap();

        info!("Starting server on {}", &address);
        let mut mount = Mount::new();
        mount.mount("/", Static::new(&self.config.paths.output));
        Iron::new(mount).http(&address).unwrap();
    }

    fn watch(&self) {
        use notify::{watcher, DebouncedEvent::*, RecursiveMode, Watcher};
        use std::sync::mpsc::channel;
        use std::time::Duration;
        info!("Watching content for changes");

        let (tx, rx) = channel();
        let mut watcher = watcher(tx, Duration::from_secs(3)).unwrap();
        watcher
            .watch(&self.config.paths.content, RecursiveMode::Recursive)
            .unwrap();

        loop {
            match rx.recv() {
                Ok(event) => match event {
                    Create(path) => self.watch_update(&path),
                    Write(path) => self.watch_update(&path),
                    Rename(_old, new) => self.watch_update(&new),
                    _ => {}
                },
                Err(e) => error!("Watch error: {:?}", e),
            }
        }
    }

    fn watch_update(&self, path: &Path) {
        let path = path
            .strip_prefix(std::env::current_dir().unwrap())
            .unwrap()
            .strip_prefix(&self.config.paths.content)
            .unwrap();
        info!("Detected change in {}", path.display());

        // TODO FIXME
        /*
        if self.is_resource(path) {
            self.add_resource(path);
            self.copy_resource(path);
            // TODO: check if any page that depends on this resource needs to
            // be rebuilt using cached parse tree.
        }
        */

        if self.is_page(path) {
            // add page to the list, if not already there
            self.add_page(path);

            // parse page and add to index
            let page = self.parse_page(&path);
            let mut pages = self.pages.write().unwrap();
            pages.push(page.clone());

            self.generate_page_html(&page);
            self.generate_page_pdf(&page);
        }
    }
}

fn hash_file<D: Digest + std::io::Write>(path: &Path) -> Result<Vec<u8>, std::io::Error> {
    let mut hasher = D::new();
    let mut file = std::fs::File::open(&path)?;
    std::io::copy(&mut file, &mut hasher)?;
    let mut output = Vec::new();
    output.extend_from_slice(hasher.finalize().as_slice());
    Ok(output)
}

struct InlineVisitor<F: FnMut(&mut pandoc_ast::Inline)> {
    function: F,
}

impl<F: FnMut(&mut pandoc_ast::Inline)> InlineVisitor<F> {
    pub fn new(function: F) -> Self {
        InlineVisitor { function }
    }
}

impl<F: FnMut(&mut pandoc_ast::Inline)> pandoc_ast::MutVisitor for InlineVisitor<F> {
    fn visit_inline(&mut self, inline: &mut pandoc_ast::Inline) {
        (self.function)(inline);
        self.walk_inline(inline);
    }
}

fn main() {
    let options = Options::from_args();

    // configure logging
    env_logger::builder()
        .format_timestamp(None)
        .filter(None, log::LevelFilter::Info)
        .init();

    std::env::set_current_dir(&options.path).unwrap();

    // read config file
    let config = std::fs::read_to_string(&options.config).unwrap();
    let config: Config = toml::from_str(&config).unwrap();

    let state = State::new(options, config);
    state.run();
}
