# xfbs.net generator

This repository contains the hacky Rust script that is the generator of my website.
It allows me to write my website in Markdown, and have it be generated to clean
HTML5 as well as to PDFs via LaTeX.

* [Documentation](https://xfbs.gitlab.io/xfbsgen/doc/xfbsgen)
* [Release (amd64)](https://xfbs.gitlab.io/xfbsgen/release/xfbsgen)

## Dependencies

This generator uses the mighty [pandoc][pandoc] for markdown processing and
converting. It also depends on LaTeX being available, as that is what pandoc
uses for PDF generation.

If you don't want to install all of these dependencies manually, you can
use a pre-existing Docker image that bundles these two dependencies. For example,
the image `thomasweise/docker-pandoc` includes everything you need.

[pandoc]: https://pandoc.org/

### Pandoc

A specific version of pandoc is required, because this generator reads in
and modifies the pandoc AST. Newer versions of pandoc might have incompatible
versions of the AST which might break things. 

You can get Pandoc installed using Cabal. Note that building it takes some time.

    apt install haskell-platform
    cabal v2-update
    cabal v2-install --install-method=copy pandoc pandoc-crossref pandoc-citeproc

If you are using Ubuntu 20.04, you might not have the latest version of Cabal
available, which might cause issues. In that case, you can use the PPA.

    add-apt-repository -y ppa:hvr/ghc
    apt update
    apt install cabal-install-3.0
    /opt/cabal/bin/cabal v2-update
    /opt/cabal/bin/cabal v2-install --install-method=copy pandoc pandoc-crossref pandoc-citeproc

The compilation of all of these packages took about 15 minutes on my machine,
which is a relatively new 8-core machine.

Both of these methods will install the binaries for `pandoc` and `pandoc-crossref`
into `~/.cabal/bin`, so make sure to add that to your path. You can do that by
running the following command in a terminal.

    export PATH="$PATH:$HOME/.cabal/bin"

### LaTeX

To get TeX Live installed, you can use the appropriate apt package. To install
the entire thing, both Debian and Ubuntu systems have the `texlive-full`
metapackage. Note that this is quite large.

    apt install texlive-full

There is also an installer for TeX Live that can be used.

### PDF2SVG

To generate SVG files from LaTeX sources, the `pdf2svg` tool is required.
This is available in most package managers, on Ubuntu or Debian systems with
APT it can be installed as such:

    apt update
    apt install pdf2svg


## How it works

When you run the Rust script, it uses pandoc to turn all of the Markdown documents
to proper HTML5 sites. I use a custom template, which defines the look of my site.

In addition to generating HTML5 output, I also generate PDFs for the people that
would like to download articles in a format that is suitable for storage.

## License

MIT license.
